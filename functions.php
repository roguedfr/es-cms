<?php
add_theme_support( 'custom-logo' );
add_theme_support( 'menus' );

function location_post() { 
	// creating (registering) the custom type 
	register_post_type( 'person', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('People', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Person', 'escms'), /* This is the individual type */
			'all_items' => __('People', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Person', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Person', 'escms'), /* Edit Display Title */
			'new_item' => __('New Person', 'escms'), /* New Display Title */
			'view_item' => __('View Person', 'escms'), /* View Display Title */
			'search_items' => __('Search People', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'People', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 100, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-universal-access', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'person', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'person', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'location_post');

function book_post() { 
	// creating (registering) the custom type 
	register_post_type( 'book', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Books', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Book', 'escms'), /* This is the individual type */
			'all_items' => __('Books', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Book', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Book', 'escms'), /* Edit Display Title */
			'new_item' => __('New Book', 'escms'), /* New Display Title */
			'view_item' => __('View Book', 'escms'), /* View Display Title */
			'search_items' => __('Search Books', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Books', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 101, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-media-document', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'book', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'book', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'book_post');

function locations_post() { 
	// creating (registering) the custom type 
	register_post_type( 'location', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Locations', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Location', 'escms'), /* This is the individual type */
			'all_items' => __('Locations', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Location', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Location', 'escms'), /* Edit Display Title */
			'new_item' => __('New Location', 'escms'), /* New Display Title */
			'view_item' => __('View Location', 'escms'), /* View Display Title */
			'search_items' => __('Search Locations', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Locations', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 102, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-admin-site-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'location', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'location', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'locations_post');

function creatures_post() { 
	// creating (registering) the custom type 
	register_post_type( 'creatures', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Creatures', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Creature', 'escms'), /* This is the individual type */
			'all_items' => __('Creatures', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Creature', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Creature', 'escms'), /* Edit Display Title */
			'new_item' => __('New Creature', 'escms'), /* New Display Title */
			'view_item' => __('View Creature', 'escms'), /* View Display Title */
			'search_items' => __('Search Creatures', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Creatures', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 107, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'creatures', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'creatures', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'creatures_post');

function city_post() { 
	// creating (registering) the custom type 
	register_post_type( 'city', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Cities', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('City', 'escms'), /* This is the individual type */
			'all_items' => __('Cities', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New City', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit City', 'escms'), /* Edit Display Title */
			'new_item' => __('New City', 'escms'), /* New Display Title */
			'view_item' => __('View City', 'escms'), /* View Display Title */
			'search_items' => __('Search Cities', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Cities', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 103, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-admin-multisite', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'city', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'city', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'city_post');

function house_post() { 
	// creating (registering) the custom type 
	register_post_type( 'house', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Houses', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('House', 'escms'), /* This is the individual type */
			'all_items' => __('Houses', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New House', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit House', 'escms'), /* Edit Display Title */
			'new_item' => __('New House', 'escms'), /* New Display Title */
			'view_item' => __('View House', 'escms'), /* View Display Title */
			'search_items' => __('Search Houses', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Houses', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 104, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'house', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'house', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'house_post');

function battles_post() { 
	// creating (registering) the custom type 
	register_post_type( 'battles', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Battles', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Battle', 'escms'), /* This is the individual type */
			'all_items' => __('Battles', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Battle', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Battle', 'escms'), /* Edit Display Title */
			'new_item' => __('New Battle', 'escms'), /* New Display Title */
			'view_item' => __('View Battle', 'escms'), /* View Display Title */
			'search_items' => __('Search Battles', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Battles', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 105, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'battles', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'battles', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'battles_post');

function others_post() { 
	// creating (registering) the custom type 
	register_post_type( 'others', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Others', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Other', 'escms'), /* This is the individual type */
			'all_items' => __('Others', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Other', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Other', 'escms'), /* Edit Display Title */
			'new_item' => __('New Other', 'escms'), /* New Display Title */
			'view_item' => __('View Other', 'escms'), /* View Display Title */
			'search_items' => __('Search Others', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Others', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 106, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'other', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'other', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'others_post');

function translation_post() { 
	// creating (registering) the custom type 
	register_post_type( 'translation', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Translations', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Translation', 'escms'), /* This is the individual type */
			'all_items' => __('Translations', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Translation', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Translation', 'escms'), /* Edit Display Title */
			'new_item' => __('New Translation', 'escms'), /* New Display Title */
			'view_item' => __('View Translation', 'escms'), /* View Display Title */
			'search_items' => __('Search Translations', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Translations', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 107, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'translation', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'translation', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'translation_post');